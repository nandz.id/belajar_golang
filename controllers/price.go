package controllers

import (
	"fmt"
	"hello/models"
	"github.com/astaxie/beego"
	"strconv"
)

type (
	PriceController struct {
		beego.Controller
	}

	ErrorJson struct {
		S []models.User
		D models.User
		F string
	}
)


func (this *PriceController) Get() {
	/*
	models.Insert(
		"name", 
		"email2@email.com", 
		"0813123456782", 
		"1234567",
	)
	*/
	id := this.GetString("id")
	int_id, err := strconv.Atoi(id)
	if(err != nil) {
		fmt.Printf("error")
	}

	models.Update(
		int_id,
		"name", 
		"ganti2@email.com", 
		"0813123456782", 
		"1234567",
	)

	a := models.GetAllUsers()
	b := models.GetUserDetail(id)	

	var responseJson ErrorJson
	responseJson = ErrorJson {
		S : a,
		D : b,
		F : id,
	}
	
	this.Data["json"] = responseJson
	this.ServeJSON()
}
